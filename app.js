require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const trucksRouter = require('./routers/trucksRouter');
const loadsRouter = require('./routers/loadsRouter');
const ResponseError = require('./errorTypes/ResponseError');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {PORT, DB_HOSTNAME, DB_NAME, DB_PASS, DB_USER} = require('./config');
const app = express();

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use(authMiddleware);
app.use('/api/users', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

app.use((err,
    req,
    res,
    next) => {
  if (err instanceof ResponseError) {
    res.status(err.errorCode).json({message: err.message});
  } else {
    res.status(500).json({message: err.message});
  }
});

const start = async () => {
  await mongoose.connect(`mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOSTNAME}/${DB_NAME}?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(PORT, ()=> console.log(`Server works at ${PORT} port!`));
};

start();
