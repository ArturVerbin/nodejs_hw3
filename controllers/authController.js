const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const generator = require('generate-password');
const {JWT_SECRET, MAIL_USERNAME, MAIL_PASS} = require('../config');
const {User} = require('../models/userModel');
const saltRounds = 10;

module.exports.login = async (req, res) => {
  const {email, password} = req.body;
  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({
      message: `No user with email ${email} found!`,
    });
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: 'Wrong password!'});
  }

  const token = jwt.sign({username: user.username, _id: user._id}, JWT_SECRET);
  res.status(200).json({jwt_token: token});
};

module.exports.registration = async (req, res) => {
  const {email, password, role} = req.body;
  const user = new User({
    email,
    password: await bcrypt.hash(password, saltRounds),
    role,
  });

  await user.save();

  res.json({message: 'User created successfully'});
};

module.exports.resetPassword = async (req, res) => {
  const {email} = req.body;
  const password = generator.generate({
    length: 10,
    numbers: true,
  });
  const user = await User.findOne({email});

  user.password = await bcrypt.hash(password, saltRounds);

  await user.save();

  await sendEmailLetter(email, password);

  res.json({message: 'New password sent to your email address'});
};

const sendEmailLetter = async (receiver, newPassword) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: MAIL_USERNAME,
      pass: MAIL_PASS,
    },
  });

  const mailOptions = {
    from: MAIL_USERNAME,
    to: receiver,
    subject: 'New password was created',
    html: `<p>Here's your new password: ${newPassword}</p>`,
  };

  await transporter.sendMail(mailOptions);
};
