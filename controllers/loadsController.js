const {Load} = require('../models/loadModel');
const ResponseError = require('../errorTypes/ResponseError');
const {isDriver} = require('../services/driverService');
const {getAssignedTruck} = require('../services/truckService');
const {
  isShipper,
  findTruck,
  createLoadLog,
  changeLoadState,
  getLoadsByUserType,
} = require('../services/shipperService');
const {
  checkLoadExists,
} = require('../services/shipperService');



module.exports.createLoad = async (req, res) => {
  await isShipper(req, res);
  const {_id} = res.locals;
  const {
    name,
    payload,
    pickup_address: pickup,
    delivery_address: delivery,
    dimensions,
  } = req.body;


  const load = new Load({
    created_by: _id,
    name,
    payload,
    pickup_address: pickup,
    delivery_address: delivery,
    dimensions,
  });

  await load.save();

  res.json({message: 'Load created successfully'});
};

module.exports.getLoads = async (req, res) => {
  const {_id} = res.locals;
  const loads = await getLoadsByUserType(req, _id);

  res.json({loads});
};

module.exports.getCurrentLoad = async (req, res) => {
  await isShipper(req, res);
  await checkLoadExists(req, res);
  const loadId = req.params.id;

  const load = await Load.findOne({_id: loadId}, {__v: 0});

  res.json({load});
};

module.exports.updateCurrentLoad = async (req, res) => {
  await isShipper(req, res);
  await checkLoadExists(req, res);
  const loadId = req.params.id;
  const {
    name,
    payload,
    pickup_address: pickup,
    delivery_address: delivery,
    dimensions,
  } = req.body;

  await Load.findByIdAndUpdate(
      {_id: loadId},
      {
        name,
        payload,
        pickup_address: pickup,
        delivery_address: delivery,
        dimensions,
      },
  );

  res.json({message: 'Load details changed successfully'});
};

module.exports.deleteCurrentLoad = async (req, res) => {
  await isShipper(req, res);
  await checkLoadExists(req, res);
  const loadId = req.params.id;
  await Load.findByIdAndDelete({_id: loadId});

  res.json({message: 'Load deleted successfully'});
};

module.exports.postLoad = async (req, res) => {
  await isShipper(req, res);
  await checkLoadExists(req, res);
  const loadId = req.params.id;
  const load = await Load.findOne({_id: loadId});
  if (load.status !== 'NEW') throw new ResponseError('You can post loads only with status "NEW"');
  load.status = "POSTED";
  createLoadLog(load, 'Load posted successfully');
  await load.save();
  const result = await findTruck(req, res);

  res.json(result);
};

module.exports.getActiveLoads = async (req, res) => {
  await isDriver(req, res);
  const {_id} = res.locals;
  const load = await Load.findOne({assigned_to: _id}, {__v: 0});
  res.json({load});
};

module.exports.getLoadShippingInfo = async (req, res) => {
  await isShipper(req, res);
  const loadId = req.params.id;

  const load = await Load.findOne({_id: loadId}, {__v: 0});
  const truck = await getAssignedTruck(load.assigned_to);
  res.json({load, truck});
};

module.exports.changeState = async (req, res) => {
  await isDriver(req, res);
  const {_id: driverId} = res.locals;

  const newState = await changeLoadState(driverId);

  res.json({message: `Load state changed to ${newState}`});
};