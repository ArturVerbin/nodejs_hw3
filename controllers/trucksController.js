const {Truck} = require('../models/truckModel');
const TRUCK_TYPES = require('../types/truckTypes');
const {
  checkTruckExist,
  checkIfTruckAssigned,
  checkIfUserAssignedTruck,
} = require('../services/truckService');
const {isDriver} = require('../services/driverService');

module.exports.getTrucks = async (req, res) => {
  await isDriver(req, res);
  const {_id} = res.locals;
  const trucks = await Truck.find({created_by: _id}, {__v: 0});

  res.json({trucks});
};

module.exports.createTruck = async (req, res) => {
  await isDriver(req, res);
  const {_id} = res.locals;
  const {type} = req.body;
  const truck = new Truck({
    created_by: _id,
    type,
    dimensions: TRUCK_TYPES[type].dimensions,
    payload: TRUCK_TYPES[type].payload
  });

  await truck.save();

  res.json({message: 'Truck created successfully'});
};

module.exports.deleteTruck = async (req, res) => {
  await isDriver(req, res);
  await checkTruckExist(req, res);
  await checkIfTruckAssigned(req, res);
  const truckId = req.params.id;

  await Truck.findByIdAndDelete({_id: truckId});

  res.json({message: 'Truck deleted successfully'});
};

module.exports.getCurrentTruck = async (req, res) => {
  await isDriver(req, res);
  await checkTruckExist(req, res);
  const truckId = req.params.id;
  const truck = await Truck.findById({_id: truckId}, {__v: 0});

  res.json({truck});
};

module.exports.updateCurrentTruck = async (req, res) => {
  await isDriver(req, res);
  await checkTruckExist(req, res);
  await checkIfTruckAssigned(req, res);
  const truckId = req.params.id;
  const {type} = req.body;

  await Truck.findByIdAndUpdate({_id: truckId}, {type});

  res.json({message: 'Truck details changed successfully'});
};

module.exports.assignTruck = async (req, res) => {
  await isDriver(req, res);
  await checkTruckExist(req, res);
  await checkIfUserAssignedTruck(req, res);
  const truckId = req.params.id;
  const {_id: userId} = res.locals;

  await Truck.findByIdAndUpdate({_id: truckId}, {assigned_to: userId});

  res.json({message: 'Truck assigned successfully'});
};
