const Joi = require('joi');
module.exports.loadValidate = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string()
        .min(0)
        .required(),
    payload: Joi.number()
        .required(),
    pickup_address: Joi.string()
        .required(),
    delivery_address: Joi.string()
        .required(),
    dimensions: Joi.object({
      width: Joi.number()
          .min(0)
          .required(),
      length: Joi.number()
          .min(0)
          .required(),
      height: Joi.number()
          .min(0)
          .required(),
    })
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};
