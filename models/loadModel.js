const mongoose = require('mongoose');

const loadModel = new mongoose.Schema({
  created_by: {
    type: String,
  },

  assigned_to: {
    type: String,
    default: null,
  },

  status: {
    type: String,
    default: 'NEW',
  },

  state: {
    type: String,
    default: null,
  },

  name: {
    type: String,
    required: true,
  },

  payload: {
    type: Number,
    required: true,
  },

  pickup_address: {
    type: String,
    required: true,
  },

  delivery_address: {
    type: String,
    required: true,
  },

  dimensions: {
    type: Object,
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
    required: true,
  },

  logs: [{
    message: {
      type: String,
      required: true,
    },
    time: {
      type: Date,
    },
  }],

  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Load = mongoose.model('Load', loadModel);
