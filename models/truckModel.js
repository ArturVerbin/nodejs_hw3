const mongoose = require('mongoose');

const truckModel = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },

  assigned_to: {
    type: String,
    default: null,
  },

  type: {
    type: String,
    required: true,
  },

  status: {
    type: String,
    default: 'IS',
  },

  dimensions: {
    type: Object,
    width: {
      type: Number,
    },
    length: {
      type: Number,
    },
    height: {
      type: Number,
    },
  },

  payload: {
    type: Number,
  },

  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Truck = mongoose.model('Truck', truckModel);
