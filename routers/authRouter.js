const express = require('express');
const router = new express.Router();
const {
  login,
  registration,
  resetPassword,
} = require('../controllers/authController');
const {asyncWrapper} = require('../helpers/helpers');
const {validateRegistration} = require('../middlewares/userValidateMiddleware');

router.post('/login', asyncWrapper(login));

router.post(
    '/register',
    asyncWrapper(validateRegistration),
    asyncWrapper(registration),
);

router.post('/forgot_password', asyncWrapper(resetPassword));

module.exports = router;