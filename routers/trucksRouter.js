const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('../helpers/helpers');
const {
  getTrucks,
  createTruck,
  deleteTruck,
  getCurrentTruck,
  updateCurrentTruck,
  assignTruck,
} = require('../controllers/trucksController');
const {truckValidate} = require('../middlewares/truckValidateMiddleware');

router.get('/',
    asyncWrapper(getTrucks),
);

router.post('/',
    asyncWrapper(truckValidate),
    asyncWrapper(createTruck),
);

router.get('/:id',
    asyncWrapper(getCurrentTruck),
);

router.put('/:id',
    asyncWrapper(truckValidate),
    asyncWrapper(updateCurrentTruck),
);

router.delete('/:id',
    asyncWrapper(deleteTruck),
);

router.post('/:id/assign',
    asyncWrapper(assignTruck),
);

module.exports = router;
