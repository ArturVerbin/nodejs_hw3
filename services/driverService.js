const {User} = require('../models/userModel');
const ResponseError = require('../errorTypes/ResponseError');

module.exports.isDriver = async (req, res) => {
  const {_id} = res.locals;
  const user = await User.findOne({_id});
  if (user.role !== 'DRIVER') {
    throw new ResponseError('Shipper can\'t do such things!');
  }
};
