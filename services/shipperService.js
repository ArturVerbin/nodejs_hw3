const {User} = require('../models/userModel');
const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');
const {LOAD_STATES} = require('../config');
const ResponseError = require('../errorTypes/ResponseError');

const MAX_LIMIT = 50;
const DEFAULT_LIMIT = 10;
const DEFAULT_OFFSET = 0;

const createLoadLog = (load,message) => load.logs.push({ message, time: Date.now() });

const isShipper = async (req, res) => {
  const {_id} = res.locals;
  const user = await User.findOne({_id});
  if (user.role !== 'SHIPPER') {
    throw new ResponseError('Driver can\'t do such things!');
  }
};

const checkLoadExists = async (req, res) => {
  const loadId = req.params.id;
  const {_id: userId} = res.locals;

  const load = await Load.findOne({_id: loadId});

  if (load.created_by !== userId) {
    throw new ResponseError('You do not have truck with this ID');
  }
};

const findTruck = async (req, res) => {
  const loadId = req.params.id;
  const load = await Load.findOne({_id: loadId});
  const truck = await Truck.findOne({})
      .where('assigned_to').ne(null)
      .where('status').equals('IS')
      .where('payload').gt(load.payload)
      .where('dimensions.width').gt(load.dimensions.width)
      .where('dimensions.length').gt(load.dimensions.length)
      .where('dimensions.height').gt(load.dimensions.height);

  if(truck) {
    load.status = "ASSIGNED";
    load.state = LOAD_STATES[0];
    load.assigned_to = truck.assigned_to;
    createLoadLog(load, `Load assigned to a driver with id ${truck.assigned_to}`);
    await load.save();
    await Truck.findByIdAndUpdate(truck._id, { status: 'OL' });
    return {message: 'Load posted successfully', driver_found: true};
  } else {
    load.status = "NEW";
    createLoadLog(load, `Driver wasn't found, load reverted to status "NEW"`);
    await load.save();
    return {message: 'Load post failed', driver_found: false};
  }
};

const changeLoadState = async (driverId) => {
  const load = await Load.where('state')
      .ne(null)
      .ne(LOAD_STATES[LOAD_STATES.length - 1])
      .findOne({ assigned_to: driverId });
  if(!load) {
    throw new ResponseError("You don't have any active load");
  }

  const stateIndex = LOAD_STATES.findIndex(state => state === load.state);
  const newState = LOAD_STATES[stateIndex + 1]
  if(stateIndex === LOAD_STATES.length - 2) {
    load.state = newState
    load.status = 'SHIPPED';
    createLoadLog(load, `Load state changed to ${newState}`);
    const truck = await Truck.findOne({assigned_to: load.assigned_to});
    truck.status = 'IS';
    await truck.save();
  } else if (stateIndex <= LOAD_STATES.length - 2) {
    createLoadLog(load, `Load state changed to ${newState}`);
    load.state = newState;
  } else {
    throw new ResponseError('This load is archived');
  }
  await load.save();
  return newState;
};

const getLoadsByUserType = async (req, userId) => {
  const {
    limit=`${DEFAULT_LIMIT}`,
    offset=`${DEFAULT_OFFSET}`,
    status,
  } = req.query;
  const user = await User.findOne({_id: userId});
  console.log(user.role);
  switch (user.role) {
    case 'DRIVER':
      const driverLoads = await Load.find(
          {assigned_to: userId},
          {__v: 0},
          {
            limit: parseInt(limit) > MAX_LIMIT ? DEFAULT_LIMIT : parseInt(limit),
            skip: parseInt(offset),
          });
      return driverLoads;

    case 'SHIPPER':
      const filters = {created_by: userId};
      if(status) {
        filters.status = status;
      }

      const shipperLoads = await Load.find(
          filters,
          {__v: 0},
          {
            limit: parseInt(limit) > MAX_LIMIT ? DEFAULT_LIMIT : parseInt(limit),
            skip: parseInt(offset),
          });

      return shipperLoads;
  }
};

module.exports = {
  isShipper,
  checkLoadExists,
  findTruck,
  createLoadLog,
  changeLoadState,
  getLoadsByUserType,
};
